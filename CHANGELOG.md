# Changelog

## [0.2.1] - 2018-11-28

* Fix length checks for arrays

## [0.2.0] - 2018-11-20

* Add batch support

## [0.1.0] - 2018-11-19

* Initial release
