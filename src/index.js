export default class AsyncJobIterator {
    constructor(options) {
        this.waitingRequests = [];
        this.completedJobs = [];
        this.isDone = false;
        this.batching = options.batching || false;
        this.batchSize = options.batchSize || 10;

        this.push = this.push.bind(this);
        this.done = this.done.bind(this);
        this.next = this.next.bind(this);
    }

    [Symbol.asyncIterator]() {
        return this;
    }

    async push(job) {
        const result = await job();

        if (!this.batching && this.waitingRequests.length > 0) {
            this.waitingRequests.shift().resolve({value: result, done: false});
            return;
        }

        this.completedJobs.push(result);

        if (this.batching && this.waitingRequests.length > 0 && this.completedJobs.length >= this.batchSize) {
            const batch = this.completedJobs.splice(0, this.batchSize);
            this.waitingRequests.shift().resolve({value: batch, done: false});
            return;
        }
    }

    async done() {
        this.isDone = true;
        if (this.batching && this.waitingRequests.length > 0 && this.completedJobs.length > 0) {
            this.waitingRequests.shift().resolve({value: this.completedJobs, done: false});
        }
        this.waitingRequests.forEach(({resolve}) => {
            resolve({done: true});
        });
    }

    async next() {
        if (this.isDone) {
            return await {
                done: true,
            };
        }
        if (this.batching) {
            if (this.completedJobs.length >= this.batchSize) {
                const batch = this.completedJobs.splice(0, this.batchSize);
                return await {
                    value: batch,
                    done: false,
                }
            }
        } else if (this.completedJobs.length > 0) {
            return await {
                value: this.completedJobs.pop(),
                done: false,
            };
        }

        return new Promise((resolve, reject) => {
            this.waitingRequests.push({resolve, reject});
        });
    }
}
